#include "console.h"
#include <algorithm>
#include <sstream>

std::string color(console_color fg_color, console_color bg_color)
{
    std::stringstream sout;

    sout << "\033[" << (fg_color + 30) << ";" << (bg_color + 40) << "m";

    return sout.str();
}

std::string clear_screen()
{
    // "\033[2J" 清空屏幕上的字符
    // "\033[3J" 清空回滚缓存区
    // "\033[H" 使光标回到左上角
    return "\033[2J\033[3J\033[H";
}
