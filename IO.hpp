#ifndef IO_HPP
#define IO_HPP

#include "console.h"
#include <boost/multiprecision/cpp_int.hpp>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <sstream>
#include <string>

/**
 * @brief 从标准输入读取一个值，并进行校验。如果值不合法，则请求重新输入。
 *
 * @tparam T 待读取值的类型，必须实现 `operator>>(istream&, T)` 运算符
 * @param predicate 对值进行校验的函数，若输入合法返回 `true`
 * @param default_value 在输入时显示的默认值。
 *                      若不需要默认值，将此项设为一个不合法输入
 * @return T 最终的合法输入结果
 */
template <typename T>
T read_num(std::function<bool(T)> predicate, T default_value)
{
    bool show_default = predicate(default_value);
    std::string s;
    do
    {
        T result = default_value;
        if (show_default)
            std::cout << color(MAGENTA) << "[" << default_value << "]";
        std::cout << color(GREEN) << "> " << color(DEFAULT);
        std::getline(std::cin, s);
        // 防止读入 EOF 字符（ANSI 终端 Ctrl+D）时进入死循环
        if (std::cin.eof())
        {
            std::cout << color(DEFAULT);
            exit(1);
        }
        std::istringstream sstream(s);
        // 防止超出 T 范围
        boost::multiprecision::uint1024_t uint1024_result = default_value;
        sstream >> uint1024_result;
        result = (T)uint1024_result;
        if (uint1024_result == result && predicate(result) && sstream.eof())
        {
            std::cout << color(DEFAULT);
            return result;
        }
        std::cout << color(RED)
                  << "您的输入有误，请重新输入。"
                     "There is an error in your input. Please enter again.\n";
    } while (!std::cin.eof());
    exit(1);
}

#endif // IO_HPP
