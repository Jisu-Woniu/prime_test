#ifndef CONSOLE_H
#define CONSOLE_H
#include <string>
/**
 * @brief 定义一系列 ANSI 终端颜色代码
 *
 */
enum console_color
{
    DEFAULT = 9,
    BLACK = 0,
    RED = 1,
    GREEN = 2,
    YELLOW = 3,
    BLUE = 4,
    MAGENTA = 5,
    CYAN = 6,
    WHITE = 7,
    BRIGHT_BLACK = 60,
    BRIGHT_RED = 61,
    BRIGHT_GREEN = 62,
    BRIGHT_YELLOW = 63,
    BRIGHT_BLUE = 64,
    BRIGHT_MAGENTA = 65,
    BRIGHT_CYAN = 66,
    BRIGHT_WHITE = 67,
};

/**
 * @brief 通过 ANSI 转义字符串改变输出文字颜色。
 *
 * @param fg_color 前景色代码
 * @param bg_color 背景色代码
 * @return std::string 生成的转义字符串，应与 std::cout 结合使用
 */
std::string color(console_color fg_color, console_color bg_color = console_color::DEFAULT);

/**
 * @brief 通过 ANSI 转义字符串清空屏幕。
 *
 * @return std::string 生成的转义字符串，应与 std::cout 结合使用
 */
std::string clear_screen();

#endif // CONSOLE_H
