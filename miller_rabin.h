#ifndef MILLER_RABIN_H
#define MILLER_RABIN_H
#include <boost/multiprecision/cpp_int.hpp>
#include <vector>

/**
 * @brief PSW 算法，即有给定基数的确定性版本的 Miller-Rabin 算法。
 *
 * @param n 要判定素性的奇数
 * @param bases 要使用的基数列表
 * @return true `n` 是基于指定基数的概率素数
 * @return false `n` 不是素数
 */
bool PSW_test(boost::multiprecision::uint128_t n,
              std::vector<boost::multiprecision::uint128_t> bases);

/**
 * @brief Miller-Rabin 算法，基于随机基数进行素性测试。
 *
 * @param n 要判定素性的奇数
 * @param s 随机生成基数的轮数
 * @return true `n` 是概率素数
 * @return false `n` 不是素数
 */
bool miller_rabin(boost::multiprecision::uint128_t n, unsigned s);

#endif // MILLER_RABIN_H
