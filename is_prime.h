#ifndef IS_PRIME_H
#define IS_PRIME_H
#include <boost/multiprecision/cpp_int.hpp>
/**
 * @brief 判断给定整数是否为素数。
 *
 * @param n 待判断整数
 * @return true 该整数为素数，对于 n > 2^83 的情况，可能有部分合数会发生误报
 * @return false 该整数为合数
 */
bool is_prime(boost::multiprecision::uint128_t n);
#endif // IS_PRIME_H
