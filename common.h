#ifndef COMMON_H
#define COMMON_H

#include <boost/multiprecision/cpp_int.hpp>
#include <boost/random.hpp>

/**
 * @brief 一个 `uint128_t` 范围的随机数生成器
 */
class rand_uint128_t
{
  private:
    /**
     * @brief 随机数生成器后端，采用梅森旋转器算法，种子来自系统熵源
     */
    static boost::random::independent_bits_engine<
        boost::random::mt19937, 128, boost::multiprecision::uint128_t>
        generator;

  public:
    /**
     * @brief 生成一个随机 `uint128_t` 整数。
     *
     * @return boost::multiprecision::uint128_t 生成的随机整数
     */
    boost::multiprecision::uint128_t operator()();

    /**
     * @brief 生成 [0, `max`) 范围的一个随机 `uint128_t` 整数。
     *
     * @param max 范围上界
     * @return boost::multiprecision::uint128_t 生成的随机整数
     */
    boost::multiprecision::uint128_t operator()(
        boost::multiprecision::uint128_t max);
    /**
     * @brief 生成 [`min`, `max`) 范围的一个随机 `uint128_t` 整数。
     *
     * @param min 范围下界
     * @param max 范围上界
     * @return boost::multiprecision::uint128_t 生成的随机整数
     */
    boost::multiprecision::uint128_t operator()(
        boost::multiprecision::uint128_t min,
        boost::multiprecision::uint128_t max);
};

/**
 * @brief 计算模取幂运算的结果
 *
 * @param base 底数
 * @param exp 指数
 * @param mod 模
 * @return boost::multiprecision::int128_t `base ** exp % mod` 的运算结果
 */
boost::multiprecision::uint128_t pow_mod(boost::multiprecision::uint128_t base,
                                         boost::multiprecision::uint128_t exp,
                                         boost::multiprecision::uint128_t mod);

/**
 * @brief 计算二进制表示中尾随零的数量
 *
 * @param n 待计算的数字
 * @return int `n` 的二进制表示中尾随零的数量
 */
int trailing(boost::multiprecision::uint128_t n);

#endif // COMMON_H
