#ifndef SEARCH_PRIME_H
#define SEARCH_PRIME_H
#include <boost/multiprecision/cpp_int.hpp>

/**
 * @brief 大于 `n` 的第 `i` 个素数
 *
 * @param n 起始数字
 * @param i 序数，默认为 1
 * @return boost::multiprecision::uint128_t 大于 `n` 的第 `i` 个素数
 */
boost::multiprecision::uint128_t next_prime(boost::multiprecision::uint128_t n,
                                            int i = 1);

/**
 * @brief 小于 `n` 的第 1 个素数
 *
 * @param n 起始数字
 * @return boost::multiprecision::uint128_t 小于 `n` 的第 1 个素数
 * @exception std::range_error 没有更小的素数
 */
boost::multiprecision::uint128_t prev_prime(
    boost::multiprecision::uint128_t n) noexcept(false);

#endif // SEARCH_PRIME_H
