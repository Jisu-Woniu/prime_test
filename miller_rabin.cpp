#include "miller_rabin.h"
#include "common.h"
using boost::multiprecision::uint128_t;

/**
 * @brief Miller-Rabin 算法的单次 witness 循环
 *
 * @param a 给定的基数
 * @param n 待测试的数
 * @param t `n - 1` 的二进制表示中尾随零数目，由调用方通过 `trailing` 函数算出
 * @param u `(n - 1) / 2 ^ t` 的结果
 * @return true `n` 是以 `a` 为基数的概率素数
 * @return false `n` 不是素数
 */
bool witness(uint128_t a, uint128_t n, int t, uint128_t u)
{
    uint128_t x = pow_mod(a, u, n);
    if (x == 1 || x == n - 1)
        return true;
    for (int i = 0; i < t; ++i)
    {
        x = pow_mod(x, 2, n);
        if (x == n - 1)
            return true;
        // 若 x + 1 !== 0 (mod n) 且 x - 1 !== 0 (mod n)，
        // 却有 x² - 1 === 0 (mod n)， 则 n 必有小于 n 的约数。
        if (x == 1)
            return false;
    }
    return false;
}

bool PSW_test(uint128_t n, std::vector<uint128_t> bases)
{
    if (n == 2)
        return true;
    if (n < 3 || !(n & 1))
        return false;
    int t = trailing(n - 1);
    uint128_t u = n >> t;
    for (uint128_t base : bases)
        if (!witness(base, n, t, u))
            return false;

    return true;
}

bool miller_rabin(uint128_t n, unsigned rounds)
{
    if (n == 2)
        return true;
    if (n < 3 || !(n & 1))
        return false;
    int t = trailing(n - 1);
    uint128_t u = (n - 1) >> t;
    rand_uint128_t rng;
    for (int i = 0; i < rounds; ++i)
    {
        uint128_t base = rng(2, n);
        if (!witness(base, n, t, u))
            return false;
    }
    return true;
}
