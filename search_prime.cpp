#include "search_prime.h"
#include "is_prime.h"
#include <stdexcept>

using boost::multiprecision::uint128_t;

int small_next[7] = {2, 2, 3, 5, 5, 7, 7};

uint128_t next_prime(uint128_t n, int i)
{
    if (i > 1)
    {
        uint128_t pr = n;

        for (int j = 0; j < i; ++j)
            pr = next_prime(pr);

        return pr;
    }
    // 较小的素数可以快速得出
    if (n < 7)
        return small_next[(int)n];

    // 对于较大的素数 p，必有 p === 1 (mod 6) 或 p === 5 (mod 6)
    uint128_t nn = 6 * (n / 6);
    if (nn == n)
    {
        ++n;

        if (is_prime(n))
            return n;

        // 下一个可能值为 nn + 5，即 n + 4
        n += 4;
    }
    else if (n - nn == 5)
    {
        n += 2;

        if (is_prime(n))
            return n;

        n += 4;
    }
    else
        n = nn + 5;

    while (true)
    {
        int tail = (int)(n % 10000);
        if (is_prime(n))
            return n;

        n += 2;

        if (is_prime(n))
            return n;

        n += 4;
    }
}

int small_prev[8] = {0, 0, 0, 2, 3, 3, 5, 5};

uint128_t prev_prime(uint128_t n) noexcept(false)
{
    if (n < 3)
        // 没有更小的素数
        throw new std::range_error("No smaller primes.");

    if (n < 8)
        return small_prev[(int)n];

    uint128_t nn = 6 * (n / 6);
    if (n - nn <= 1)
    {
        n = nn - 1;

        if (is_prime(n))
            return n;

        n -= 4;
    }
    else
        n = nn + 1;

    while (true)
    {
        if (is_prime(n))
            return n;

        n -= 2;

        if (is_prime(n))
            return n;

        n -= 4;
    }
    return n;
}
