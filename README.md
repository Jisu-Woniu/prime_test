# Prime Test

## 如何构建本项目

本项目以 Ubuntu 22.10 作为主要开发、测试环境，但由于采用跨平台工具链，以下步骤在 Windows 依然可用。

1.  安装最新版 [CMake](https://cmake.org)

2.  在项目目录下执行 `./vcpkg/bootstrap-vcpkg.(bat/sh)`

3.  在项目目录下执行 `./vcpkg/vcpkg install` 以安装依赖

4.  在项目目录下执行 `cmake-gui -B build -S . -DCMAKE_TOOLCHAIN_FILE=./vcpkg/scripts/buildsystems/vcpkg.cmake`，若提示 Boost 未找到，可手动将路径设置为 vcpkg 安装目录下的 `include` 目录

5.  使用 Visual Studio 或 make 编译、构建

6.  若在 Windows 下运行，请确保终端支持 ANSI 转义序列，并支持 UTF-8 编码输出（需要 Windows 10 1511 以上版本，或使用基于 MinTTY 的终端如 Git Bash），否则可能导致终端不能正常显示颜色、字符乱码等情况

## License

本应用采用 [GNU 通用公共许可证 3.0 或更新版本（GPL-3.0-or-later）](./LICENSE)开源，二次分发请提供源代码。

[![GPL-3.0-or-later](https://www.gnu.org/graphics/gplv3-or-later.svg)](./LICENSE)
