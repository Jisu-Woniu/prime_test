#ifndef RANDOM_PRIME_H
#define RANDOM_PRIME_H

#include <boost/multiprecision/cpp_int.hpp>

/**
 * @brief 生成 [2, `max`) 范围一个的随机素数。
 *
 * @param max 待生成素数范围上界
 * @return boost::multiprecision::uint128_t 生成的随机素数
 */
boost::multiprecision::uint128_t random_prime(
    boost::multiprecision::uint128_t max) noexcept(false);

/**
 * @brief 生成 [`min`, `max`) 范围一个的随机素数。
 *
 * @param min 待生成素数范围下界
 * @param max 待生成素数范围上界
 * @return boost::multiprecision::uint128_t 生成的随机素数
 * @exception std::invalid_argument("Invalid range.") 区间不合法
 *            std::range_error("No primes in range.") 指定区间内没有素数
 */
boost::multiprecision::uint128_t random_prime(
    boost::multiprecision::uint128_t min,
    boost::multiprecision::uint128_t max) noexcept(false);

#endif // RANDOM_PRIME_H
