#include "is_prime.h"
#include "common.h"
#include "miller_rabin.h"
#include <vector>

using boost::multiprecision::uint128_t;
bool is_prime(uint128_t n)
{
    using namespace boost::multiprecision::literals;
    if (n == 2 || n == 3 || n == 5)
        return true;
    if (n < 2 || n % 2 == 0 || n % 3 == 0 || n % 5 == 0)
        return false;
    if (n < 49)
        return true;
    if ((n % 7) == 0 || (n % 11) == 0 || (n % 13) == 0 || (n % 17) == 0 ||
        (n % 19) == 0 || (n % 23) == 0 || (n % 29) == 0 || (n % 31) == 0 ||
        (n % 37) == 0 || (n % 41) == 0 || (n % 43) == 0 || (n % 47) == 0)
        return false;
    if (n < 2809)
        return true;
    if (n < 31417)
        // 使用 Fermat 素性测试并排除已知的 Fermat 伪素数
        return pow_mod(2, n, n) == 2 && n != 7957 && n != 8321 && n != 13747 &&
               n != 18721 && n != 19951 && n != 23377;

    // 使用确定性 Miller-Rabin 素性测试算法
    if (n < 1373653)
        return PSW_test(n, std::vector<uint128_t>{2, 3});
    if (n < 9080191)
        return PSW_test(n, std::vector<uint128_t>{31, 73});
    if (n < 25326001)
        return PSW_test(n, std::vector<uint128_t>{2, 3, 5});
    if (n < 3215031751)
        return PSW_test(n, std::vector<uint128_t>{2, 3, 5, 7});
    if (n < 4759123141)
        return PSW_test(n, std::vector<uint128_t>{2, 7, 61});
    if (n < 1122004669633)
        return PSW_test(n, std::vector<uint128_t>{2, 13, 23, 1662803});
    if (n < 2152302898747)
        return PSW_test(n, std::vector<uint128_t>{2, 3, 5, 7, 11});
    if (n < 3474749660383)
        return PSW_test(n, std::vector<uint128_t>{2, 3, 5, 7, 11, 13});
    if (n < 341550071728321)
        return PSW_test(n, std::vector<uint128_t>{2, 3, 5, 7, 11, 13, 17});
    if (n < 3825123056546413051)
        return PSW_test(
            n, std::vector<uint128_t>{2, 3, 5, 7, 11, 13, 17, 19, 23});
    if (n < 0x437ae92817f9fc85b7e5_cppui)
        return PSW_test(n, std::vector<uint128_t>{2, 3, 5, 7, 11, 13, 17, 19,
                                                  23, 29, 31, 37});
    if (n < 0x2be6951adc5b22410a5fd_cppui)
        return PSW_test(n, std::vector<uint128_t>{2, 3, 5, 7, 11, 13, 17, 19,
                                                  23, 29, 31, 37, 41});
    return miller_rabin(n, 20);
}
