#include "random_prime.h"
#include "common.h"
#include "is_prime.h"
#include "search_prime.h"
#include <stdexcept>

using boost::multiprecision::uint128_t;

uint128_t random_prime(uint128_t max) noexcept(false)
{
    return random_prime(2, max);
}

uint128_t random_prime(uint128_t min, uint128_t max) noexcept(false)
{
    if (min >= max)
        throw new std::invalid_argument("Invalid range.");

    rand_uint128_t rng;
    uint128_t r = rng(min, max);
    if (is_prime(r))
        return r;

    uint128_t next = next_prime(r);
    if (next < max)
        return next;

    try
    {
        uint128_t prev = prev_prime(r);
        if (prev >= min)
            return prev;
    }
    catch (std::range_error *ex)
    {
        // 内层错误不影响函数结果
        delete ex;
    }

    throw new std::range_error("No primes in range.");
}
