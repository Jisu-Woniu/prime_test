#include "IO.hpp"
#include "console.h"
#include "is_prime.h"
#include "random_prime.h"
#include "search_prime.h"
#include <chrono>
#include <exception>
#include <iomanip>
#include <iostream>
#include <thread>

using boost::multiprecision::uint128_t;
using namespace boost::multiprecision::literals;

int main()
{
    std::cout << clear_screen() << color(GREEN)
              << "╔════════════════════════════════════════╗\n"
              << "║          " << color(BLUE) << "欢迎来到素数的世界！"
              << color(GREEN) << "          ║\n"
              << "║    " << color(BLUE) << "Welcome to the world of primes!"
              << color(GREEN) << "     ║\n"
              << "╚════════════════════════════════════════╝" << color(DEFAULT)
              << "\n\n";
    while (true)
    {
        std::cout << color(BLUE) << "请选择需要的操作。\n"
                  << color(BLUE) << "Please choose what you want to do.\n"
                  << color(GREEN) << "1 - " << color(BLUE)
                  << "生成一个随机素数。" << color(BLUE)
                  << "Generate a random prime.\n"
                  << color(GREEN) << "2 - " << color(BLUE)
                  << "测试整数是否为素数。" << color(BLUE)
                  << "Test whether a number is a prime.\n"
                  << color(GREEN) << "3 - " << color(BLUE)
                  << "验证哥德巴赫猜想。" << color(BLUE)
                  << "Verify Goldbach's conjecture.\n"
                  << color(GREEN) << "0 - " << color(BLUE) << "退出。"
                  << color(BLUE) << "Exit\n"
                  << color(DEFAULT);
        int i = read_num<int>([](int n) { return n >= 0 && n <= 3; }, 0);
        uint128_t n, m;
        switch (i)
        {
        case 0:
            std::cout << color(BLUE) << "再见！" << color(BLUE) << "Bye!"
                      << std::endl;
            std::cout << color(DEFAULT);
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(1s);
            return 0;
        case 1:
            std::cout << color(BLUE)
                      << "请输入范围下界，应小于 2^128。[默认：2]\n"
                         "Please enter the lower bound of the range, "
                         "it should be less than 2^128. [default: 2]\n";
            n = read_num<uint128_t>([](uint128_t n) { return n > 0; }, 2);
            std::cout
                << color(BLUE)
                << "请输入范围上界，应小于 2^128。\n"
                   "Please enter the upper bound of the range, "
                   "it should be less than 2^128.\n"
                   "建议上界应至少为下界的两倍。[默认：2 * 下界]\n"
                   "It is recommended "
                   "that the number be no less than twice the lower bound. "
                   "[default: 2 * lower bound]\n";
            m = read_num<uint128_t>([&](uint128_t m) { return m >= n; },
                                    n << 1);
            try
            {
                uint128_t r = random_prime(n, m + 1);
                std::cout << color(BLUE)
                          << "生成的随机素数是：The random prime is: "
                          << color(MAGENTA) << r << std::endl;
            }
            catch (std::exception *ex)
            {
                std::cout << color(BRIGHT_RED) << "错误：Error:\n"
                          << ex->what();
                delete ex;
            }
            std::cout << color(DEFAULT) << std::endl;
            break;
        case 2:
            std::cout << color(BLUE)
                      << "请输入小于 2^128 的整数。"
                         "Please enter a number less than 2^128."
                      << color(DEFAULT) << std::endl;
            n = read_num<uint128_t>([](uint128_t n) { return n > 0; }, 0);
            if (is_prime(n))
            {
                std::cout << color(BLUE) << n << "是素数。" << n
                          << " is a prime." << color(DEFAULT) << std::endl;
            }
            else
            {
                std::cout << color(BLUE) << n << "不是素数。" << n
                          << " is not a prime." << color(DEFAULT) << std::endl;
            }
            std::cout << color(DEFAULT);
            break;
        case 3:
            std::cout << color(BLUE)
                      << "输入待验证范围上界。[默认：1000]"
                         "Please enter the upper bound of the range "
                         "to validate. [default: 1000]\n";
            n = read_num<uint128_t>([](uint128_t n) { return n >= 4; }, 1000);
            for (uint128_t i = 4; i <= n; i += 2)
            {
                for (uint128_t j = 2; (j << 1) <= i; j = next_prime(j))
                {
                    if (is_prime(i - j))
                    {
                        std::cout << color(GREEN) << i << color(MAGENTA)
                                  << " = " << color(GREEN) << j
                                  << color(MAGENTA) << " + " << color(GREEN)
                                  << i - j << "\n"
                                  << color(DEFAULT);
                        goto next_i;
                    }
                }
                std::cout << color(BLUE) << "对于 " << i
                          << " 没有合适的解。\nNo proper answer for " << i
                          << "." << std::endl;
                break;
            next_i:;
            }
            std::cout << color(DEFAULT);
            break;
        }
        std::cout << "═══════════════════════════\n";
    }
}
