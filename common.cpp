#include "common.h"
#include <random>

using boost::multiprecision::uint128_t;
using boost::random::independent_bits_engine;
using boost::random::mt19937;
using boost::random::uniform_int_distribution;

independent_bits_engine<mt19937, 128, uint128_t> rand_uint128_t::generator =
    independent_bits_engine<mt19937, 128, uint128_t>(std::random_device()());

uint128_t rand_uint128_t::operator()()
{
    return generator();
}

uint128_t rand_uint128_t::operator()(uint128_t max)
{
    return uniform_int_distribution<uint128_t>(0, max - 1)(generator);
}

uint128_t rand_uint128_t::operator()(uint128_t min, uint128_t max)
{
    return uniform_int_distribution<uint128_t>(min, max - 1)(generator);
}

/**
 * @brief [0, 256) 所有整数的二进制尾随零个数，使用代码预先生成备用。
 */
int small_trailing[256] = {
    0, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0,
    3, 0, 1, 0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
    4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 6, 0, 1, 0, 2, 0, 1, 0,
    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
    5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0,
    3, 0, 1, 0, 2, 0, 1, 0, 7, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
    4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0,
    3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
    6, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0, 4, 0, 1, 0, 2, 0, 1, 0,
    3, 0, 1, 0, 2, 0, 1, 0, 5, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0,
    4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};

uint128_t pow_mod(uint128_t base, uint128_t exp, uint128_t mod)
{
    if (base == 1 || exp == 0)
        return 1;

    boost::multiprecision::uint256_t result = 1, b256 = base % mod;

    while (exp)
    {
        if (exp & 1)
            result = (result * b256) % mod;
        exp >>= 1;
        b256 = (b256 * b256) % mod;
    }
    return (uint128_t)result;
}

int trailing(uint128_t n)
{
    if (n == 0)
        return 0;
    int low_byte = (int)(n & 0xff);
    if (low_byte)
        return small_trailing[low_byte];
    int t = 8;
    n >>= 8;
    while (!(n & 0xff))
    {
        n >>= 8;
        t += 8;
    }
    return t + small_trailing[(int)(n & 0xff)];
}
